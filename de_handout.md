# Google Protobuf
​
Protocol Buffers (protobuf) ist ein Datenformat zur Serialisierung mithilfe einer Schnittstellen-Beschreibungssprache (IDL), welche von Google entwickelt und zum Teil unter einer 3-Klausel-BSD-Lizenz veröffentlicht wurde. Durch das unabhängige Datenformat werden alle gängigen Programmiersprachen wie bspw. C#, C++, Dart, Go, Objective-C, Java, Javascript, Python und Kotlin offiziell unterstützt. Darüberhinaus gibt es Projekte von dritten, die eine Einbidung in C, Swift, Lua, Matlab, PHP, TypeScript uvm. ermöglichen. 
​
Protobuf wurde 2001 für interne Zwecke von Google entwickelt und am 07. Juli 2008 für die öffentlichkeit bereitgestellt. Aktuell steht das Repository als Open-Source Projekt auf Github zur Verfügung (https://github.com/protocolbuffers/protobuf). Das Letzte Stable release (v3.17.0) wurde am 13.05.2021 veröffentlicht. 
​
## Warum sollten Sie Protobuf verwenden?
Um uns für Protobuf zu motivieren, brauchen wir gar nicht viel. Es reicht bereits ein einfaches Beispiel: "Addressbuch". 
Wir gehen davon aus, dass in diesem Addressbuch mehrere Personen, sowie die folgenden Eigenschaften einer person gespeichert werden:
- ID
- Name
- Vorname
- Email
- Telefonnummer
​
Das beispiel wird in den folgenden Themen als Grundlage verwendet.
Nun stellt man sich die Frage, wie können diese Daten schnell und einfach verschickt werden?:
- In C# gibt es beispielsweise die Möglichkeit ein einfaches Objekt zu serialisieren und zu versenden. Mit der Zeit ist dies ein sehr instabiler Ansatz, denn der empfänger muss zu jeglicher Zeit dasselbe Speichermodell der Datenstruktur aufweisen, sowie die Binäre-Formatierung von C# verstehen. Abwärtskompatibilität ist hier bspw. nicht mehr möglich. 
- Eine weitere Möglichkeit wäre sein eigenes Format zu erstellen und dementsprechend einen kleinen Parser zur Verfügung stellen. In einfachen Fällen wie bspw."\<id>:\<vorname>:\<name>:\<email>:\<phonenumber>;\<id>:\<vorname>:\<name>:\<email>:\<phonenumber>;" könnte das noch gut funktionieren. Aber sobald komplexe Datestrukturen mit Sub-Typen ins Spiel kommen, muss unter Umständen ein enormer Aufwand betrieben werden um den Parser aufzubauen und dementsprechend zu pflegen. Möglicherweise ist es dann auch noch erforderlich eine Implementierung für eine andere Programmiersprache umzusetzen. Was bei mindestestens zwei Programmiersprachen einen doppelten Pflegeaufwand bedeutet. 
- Ein weiteres sehr beliebtes Austauschformat ist XML. Es ist für den Menschen gut lesbar und stellt durch das Schema eine Daten-Konsitenz sicher. Desweiteren bietet XML den Vorteil, dass es in sämtlichen Programmiersprachen relativ einfach zu lesen und schreiben ist. Ein großer Nachteil ist der XML-DOM, dieser kann sehr groß und unübersichtlich werden, was die Performance der Applikation unter Umständen stark beeinträchtigen kann. 
- Ein weiteres und ebenfalls sehr beliebtes Format ist JSON. Ebenso in fast allen Programmiersprachen vertreten und für das menschliche Auge einfach zu lesen. Bis zu einer gewissen Größe gut zu verarbeiten, aber irgendwann merkt man einbußen an der Performance. 

### Performance Protobuf vs. JSON
Im folgenden der Vergleich zwischen Protobuf (Binary-Format) und diversen JSON-Formattern.
- Google.Protobuf
- Google.Protobuf (integrierter JSON-Formatter)
- Newtonsoft.Json
- System.Text.Json (C# ab .NetCore 3.1)

Das nachfolgende Diagramm zeigt die serialisierte größe (in Kilobyte) für eine bestimmte Anzahl von Personen im Adressbuch. Dabei ist sehr deutlich zu erkennen, dass das Binär-Format ungefähr halb so viel Speicher im Vergleich zu den JSON-Formattern benötigt (das JSON wurde dabei nicht komprimiert). In den unteren Bereichen ist der Unterschied unscheinbar (dennoch in etwa die Hälfte), aber sobald die Datenmenge stark zunimmt, ist der unterschied sehr deutlich zu erkennen.

​![Serialization size protobuf vs. JSON](bench_size.png)

Das nächste Bild zeigt einen Vergleich zwischen Protobuf und JSON bei der Serialisierung. Als Basis wurde auch hier ein Adressbuch mit bis zu 4.000.000 Personen erstellt und im Anschluss mit den unterschiedlichen Mitteln serialisiert und dabei die Zeit gemessen. Es ist in diesem Schaubild sehr deutlich zu erkennen, dass bis zu 100.000 Objekten kein gravierender unterschied besteht. Erst im 7-stelligen Bereich ist es von Schritt zu Schritt sehr deutlich zu erkennen, dass Protobuf für 4.000.000 Personen im Adressbuch geschlagene 3,8 sekunden, während der schnellste JSON-Serializer bereits 11,1 Sekunden benötigt. In Prozent bedeutet das, das Protobuf um 294% schneller ist, als JSON.

​![Serialization time protobuf vs. JSON](bench_time.png)

Zusammengefasst kommt es immer auf den Anwendungsfall an. Wenn von vorherein bekannt, dass die Menge der Daten überschaubar ist, kann frei ausgewählt werden. Sobald aber bekannt ist, dass es sich um Große Datenmengen handelt, sollte man auf ein Binär-Format zurückgreifen. Diese sind in puncto Performance oft im Vorteil.

##  Wie erstellt man eine Nachricht?

Jetzt stellt sich die Frage, wie wir überhaupt so eine Protobuf-Message erstellen können? Dafür hat Google eine Schnittstellen-Beschreibungssprache eingeführt, welche die unabhängigkeit zu einer Programmiersprache sicherstellt.

```protobuf
syntax = "proto3";

package Example;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string description = 1;
    string telephoneNumber = 2;
  }
}
```
In diesem Beispiel wird Protobuf mit der Version 3 (proto3) verwendet. Im obigen Beispiel wird ein `Addressbook` mit einer Liste von Personen erstellt. In jeder `Person` eine `Id`, `Name`, `FirstName`, `Email` und eine Liste von Telefonnummern mit jeweils einer `Description` und einer `TelephoneNumber`. 

An dieser stelle ist es wichtig zu erwähnen, dass nur der Datentyp und die Namen der Felder für den Protobuf-Compiler wichtig sind. Für die Serialisierung ist ausschließlich die Eindeutigenummer (hinter dem "=") von Bedeutung. Dementsprechend ist es für die Performance von Vorteil wenn zu Beginn die Zahlen zwischen 1 und 15 gewählt werden. Diese benötigen nämlich nur ein Byte. Alle Zahlen zwischen 16 und 2047 benötigen dann schon zwei Byte. Maximal ist die Zahl 2^29-1 bzw. 536.870.911 möglich. Dementsprechend gilt: Je größer die Feld-Nummer desto größer wird auch die Größe der Nachricht. Für diejenigen die mehr Informationen zur Serialisierung haben möchten, können sich den Guide von Google ansehen (https://developers.google.com/protocol-buffers/docs/encoding). Dort wird im Detail beschrieben wie das Binär-Format aufgebaut ist.

### Skalare Datentypen
Protobuf stellt alle wichtigen Datentypen zur Verfügung (mehr infos unter: https://developers.google.com/protocol-buffers/docs/proto3#scalar). Dort ist auch im Genauen definiert, auf welche Datentypen in den unterschiedlichen Programmiersprachen gemappt wird (C# Beispiel: **int64 => long**). 

### Enumerationen
Enum's wurden ebenfalls in Protobuf implementiert (https://developers.google.com/protocol-buffers/docs/proto3#enum). Diese sehen strukturell sehr ähnlich aus wie in gängigen Programmiersprachen. Allerdings gibt es zwei Beschränkungen:
- Es muss eine Konstante mit dem Wert 0 geben, als numerischer Standardwert.
- Zustzlich um dem Schema von Version 2 kompatibel zu bleiben, muss die Konstante mit dem Wert 0 an der ersten Stelle platziert werden.

Für die Serialisierung ist auch nur der Wert von Bedeutung. Der Compiler nutzt den Namen um die entsprechende Datenstruktur der jeweiligen Programmiersprache zu erstellen. Bei der Benennung ist es wichtig auf den Styling Guide von Google Protobuf (https://developers.google.com/protocol-buffers/docs/style) zu achten, ansonsten kann es bspw. zu kollissionen mit Sprachspezifischen Schlüsselwörtern kommen. Im folgenden ein Beispiel, das unbedingt vermieden werden sollte.

#### Schlechtes Beispiel

```protobuf
// schlechtes Beispiel
syntax = "proto3";

package EnumExample;

message EnumExampleMessage {
  ...
  enum FieldTypes {
    int = 0;
    double = 1;
    float = 2;
  }
  ...
}
```
Der folgende generierte Java-Code führt zu einem Compiler-Fehler, weil Schlüsselwörter (int, double, float) verwendet werden, die von Java geschützt sind. Für den Fall das man nur der anbindende der Schnittstelle ist und keinen unmittelbaren Einfluss auf die Benennung hat, kann man den workaround gehen und die Konstanten in der proto-Datei umbenennen. Wichtig dabei ist, dass die Werte dieselben bleiben. Das Ziel sollte allerdings sein, das solche workarounds vermieden werden.
```java
// Generiertes Java Beispiel 
public enum FieldTypes implements com.google.protobuf.ProtocolMessageEnum {
    /**
    * <code>int = 0;</code>
    */
    int(0),
    /**
    * <code>double = 1;</code>
    */
    double(1),
    /**
    * <code>float = 2;</code>
    */
    float(2)
    ...
}
```
In C# dagegen, würde das obige Beispiel funktionieren.
```csharp
....
 public static partial class Types {
    public enum FieldTypes {
        [pbr::OriginalName("int")] Int = 0,
        [pbr::OriginalName("double")] Double = 1,
        [pbr::OriginalName("float")] Float = 2,
    }
}
....
```

#### Gutes Beispiel
Es empfiehlt sich eine `DEFAULT`-Konstante dem 0-Wert zuzuordnen. Dass wenn kein Wert angegeben wurde, dieser erkannt wird. 
```protobuf
// Gutes Beispiel
syntax = "proto3";

package EnumExample;

message EnumExampleMessage {
  ...
  enum FieldTypes {
    DEFAULT = 0;
    INT = 1;
    DOUBLE = 2;
    FLOAT = 3;
  }
  ...
}
```
Daraus resultiert ein valider Java-Code.
```java
// Generiertes Java Beispiel 
public enum FieldTypes implements com.google.protobuf.ProtocolMessageEnum {
    /**
    * <code>DEFAULT = 0;</code>
    */
    DEFAULT(0),
    /**
    * <code>INT = 1;</code>
    */
    INT(1),
    /**
    * <code>DOUBLE = 2;</code>
    */
    DOUBLE(2),
    /**
    * <code>FLOAT = 3;</code>
    */
    FLOAT(3)
    ...
}
```
In C# resultiert mehr oder weniger dasselbe wie im schlechten Beispiel.
```csharp
....
public static partial class Types {
    public enum FieldTypes {
        [pbr::OriginalName("DEFAULT")] Default = 0,
        [pbr::OriginalName("INT")] Int = 1,
        [pbr::OriginalName("DOUBLE")] Double = 2,
        [pbr::OriginalName("FLOAT")] Float = 3,
    }
}
....
```

## Wie behandeln Sie die Abwärtskompatibilität in Ihrem Nachrichtenformat?

Wenn es um das Thema Abwärtskompatibilität geht, ist Protobuf ein sehr gutes Beispiel. Es gibt nur wenige Punkte zu beachten, die dafür Sorge tragen, dass die Nachrichten Abwärtskompatibel bleiben (https://developers.google.com/protocol-buffers/docs/proto3#updating). 
- Die eindeutige Nummer von einem existierenden Feld sollte nicht verändert werden.
- Wenn neue Felder hinzugefügt werden, so werden diese von den Programmen, welche das alte Format verwenden einfach ignoriert.  
- Bestehende Felder können gelöscht werden, solange die Nummer nicht neu vergeben wird. Um eine neuvergebung der Nummer zu vermeiden, sollte das Feld als `reserved` markiert werden.
- Die Datentypen wie int32, uint32, int64, uint64 und bool sind kompatibel. Das heißt, es ist immer möglich diese Datentypen untereinander zu tauschen ohne das es zu Kompatibilitätsproblemen kommt. Denn bei numerischen Werten wird einfach abgeschnitten, wenn es nicht rein passt. Als Beispiel: Wenn man versucht in C++ eine 64-bit Zahl in einen 32-bit Datentyp zu speichern, wird das Ende einfach abgeschnitten.
- sint32 und sint64 sind untereinander kompatibel aber nicht mit anderen int-Typen.
- string und bytes sind kompatibel solange es sich um gültiges UTF-8 handelt.

Es gibt noch ein paar mehr Punkte zu beachten, aber die können in der Dokumentation von Google Protobuf nachgelesen werden (https://developers.google.com/protocol-buffers/docs/proto3#updating). 

### Einfaches Beispiel
Im nachfolgenden ein einfaches Beispiel zur Abwärtskompatibilität. Als Beispiel nehme ich hier das Adressbuch zur Hand.
```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string description = 1;
    string telephoneNumber = 2;
  }
}
```
In dem Sub-Type `TelephoneNumber` soll das Feld `description = 1` durch ein Enum ausgetauscht werden.  
```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string telephoneNumber = 2;
    NumberType numberType = 3;

    enum NumberType {
      DEFAULT = 0;
      HOME = 1;
      WORK = 2;
      MOBILE = 3;
      WORK_MOBILE = 4;
    }

    reserved 1; // mark 1 as reserved that it will never used again
  }
}
```
Wenn beispielsweise ein Service auf ein neues format umstellt, kann der Client trotzdem noch mit dem Server kommunizieren. In diesem Fall kommt es zum Verlust der Information, um welchen Typ Nummer es sich handelt und wird dementsprechend auf Server-Seite standardmäßig als Default eingestuft. Bei Nachrichten vom neuen Server Format auf einen alten Client, fehlt dem Client der Inhalt der Beschreibung. Alternativ kann das Feld auch mit einem Präfix versehen werden.

```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string OBSOLETE_description = 1;
    string telephoneNumber = 2;
    NumberType numberType = 3;

    enum NumberType {
      DEFAULT = 0;
      HOME = 1;
      WORK = 2;
      MOBILE = 3;
      WORK_MOBILE = 4;
    }
  }
}
```
Dementsprechend kann das Feld vom Server mit einem Wert versehen werden. So könnte auf Server-Ebene der Name der Enum-Konstante mit `.ToString()` an den Client verschickt werden.  


## Wie kann man das nicht menschenlesbare Format debuggen?
Das debuggen auf unterster Ebene, als auf Byte-Ebene, ist relativ aufwändig und für das menschliche Auge nur mit unterstützung möglich.
Im folgenden werden zwei Methoden beschrieben. Die erste methode basiert auf Byte-Ebene und wird mithilfe von Base64 und einem Tool dargstellt und bietet die Möglichkeit einer strukurellen analyse. Die zweite vorgehensweise ist für das menschliche Auge etwas angenehmer. Ist aber erst nach dem Deserialisieren möglich und beschränkt sich auf die Nutzdaten (dementsprechend keine strukurelle Analyse mögliche). 

### 1. Variante - Byte-Ebene (strukturell)
Wenn Protobuf Nachrichten am Clienten ankommen, sind diese in ein Byte-Array verpackt. Diese Daten mit dem menschlichen Auge zu analysieren ist zwar möglich, aber extrem anstrengend. Deswegen ist es von Vorteil die Byte-Daten in Base64 konvertieren. So kann es beispielsweise aus einem Log-File herauskopiert werden und in das folgende Tool eingesetzt werden.

Zu Beginn brauchen wir zwei Helper-Methoden für die Konvertierung in einen Base64-String (Beispiel C#).

```csharp
.... 
// Helpers
public static string ByteDataToBase64(byte[] data)
{
    if (data.Length == 0)
    { // there are no Content
        return "==== Protobuf message empty ===";
    }

    return Convert.ToBase64String(data);
}

public static string ProtobufMessageToBase64(IMessage message)
{
    return ByteDataToBase64(message.ToByteArray());
}
...
```

Im folgenden wird eine Protobuf Nachricht erzeugt und anschließend in eine Base64-Zeichenkette konvertiert. In einer Produktions-Umgebung würden die Byte-Daten über einen Websocket ankommen. Der ein­fach­heits­hal­ber wird erste eine Message befüllt und im Anschluss serialisiert. Am Ende des folgenden Code-Ausschitts ist der Base64-String zu sehen und dieser muss nun in das Tool eingesetzt werden.
```csharp
...
// For example we create a message and use the binary data to analyze.
public void BinaryDataExample() {
    Addressbook addressbook = new Addressbook();
    var person = new Person
    {
        Id = 1,
        FirstName = "John",
        Name = "Doe",
        Email = "john.doe@unknown.com",
    };
    person.Numbers.Add(new Person.Types.TelephoneNumber { Description = "Work", TelephoneNumber_ = "1234/7382947" });
    addressbook.Persons.Add(person);

    var binaryData = addressbook.ToByteArray();
    // Convert to Base64
    var base64Data = ProtobufMessageToBase64(binaryData); 
    // => base64Data = CjkIARIDRG9lGgRKb2huIhRqb2huLmRvZUB1bmtub3duLmNvbSoUCgRXb3JrEgwxMjM0LzczODI5NDc= 
}
...
```

Das Tool ist online unter https://protogen.marcgravell.com/decode verfügbar. Dieses Werkzeug untestützt das Eingabe-Format einer Datei, Base64 und Hexa-Dezimal. Das Tool ist zwar online verfügbar aber die Deserialisierung läuft komplett im Browser. Das heißt, es findet währenddessen keine Kommunikation mit dem Server statt. Dementsprechend wäre es auch möglich sensible Daten auszuwerten. Um eine unerwartete Kommunikation auszuschließen, kann man sich offline stellen (Entwicklertools vom Browser). So kann definitv keine Verbindung mit dem Server hergestellt werden. Im folgenden nun ein Bild vom "Online Protobuf Decoder". Dieser ist in der Lage ohne das Schema der eigentlichen Protobuf-Message die Daten hierarchisch darzustellen. Mit allen Information die der Parser aus den Byte-Daten auslesen kann. Im Vergleich das dazugehörige Schema. Es ist sehr schön zu erkennen, dass es diverse sub-objekte gibt. In diesem Beispiel handelt es sich um die Personen bzw. Telefon-Nummern, wobei die Nummern wieder sub-objekte der Personen sind. Die in dem Bild dargestellten Feld-Nummern (`Field #1`, `Field #2`, ...) entsprechen den definierten im Schema. 

​![Serialization size protobuf vs. JSON](proto_byte_debug.png)

### 2. Variante - Nutzdaten
Mit der zweiten Variante hat man nur bedingt die Möglichkeit auf strukturelles Debugging. Das heißt, es kann im wesentlichen nur ein Debugging der Nutzdaten durchgeführt werden. Dazu wird die Nachricht mit dem erwarteten Schema geparsed und im Anschluss in JSON konvertiert. So kann analysiert werden, ob der Inhalt der Daten dem entspricht was erwartet wird. 

> **Wichtig**: An dieser Stelle ist es wichtig zu erwähnen, dass die Konvertierung am Besten nur dann durchgeführt wird, wenn man explizit die Nachrichten debuggen möchte (bspw. wenn das LogLevel auf Debug gestellt ist). Ansonsten kann es zu beeinträchtigungen der Performance kommen und der Vorteil von Protobuf wird vernichtet. 

```csharp
...
void DebugData(byte[] byteData, ILogger logger) {
    // only while debug enabled or any other indicator
    if (logger.IsDebug) {
        // parse data with expected message
        Addressbook addressbook = Addressbook.Parser.ParseFrom(byteData);
        // use the built-in json formatter from Protobuf or anyone else.
        JsonFormatter formatter = new JsonFormatter(new JsonFormatter.Settings(true));
        string jsonData = formatter.Format(addressbook);
        // Log data
        logger.Debug(jsonData);
    } 
}
...
```
Ausgabe:
```json
{ 
    "persons": [ 
        { 
            "id": 1, 
            "name": "Doe", 
            "firstName": "John", 
            "email": "john.doe@unknown.com", 
            "numbers": [ 
                { 
                    "description": "Work", 
                    "telephoneNumber": "1234/7382947" 
                }
            ] 
        } 
    ] 
}
```
Hierbei kann man sich nun alle Daten in Ruhe ansehen und auf Fehler überprüfen. 

## Zusammenfassung
Abschließend kann gesagt werden, dass Protobuf eine sehr einfache und performante Library ist um Daten schnell und sicher an das Ziel zu übermitteln. Wenn man im Internet diverse Beiträge zu dem Thema liest, wird oft der negative Aspekt genannt, dass die Community natürlich nicht ganz so groß ist, als die von JSON. Allerdings muss auch dazu gesagt werden, das in vielen Fällen JSON auch vollkommen ausreichend ist. Dementsprechend ist es ganz klar das die Community um ein vielfaches größer ist. Trotz alledem gibt es für Protobuf viele Bibliotheken und auch einige Tutorials die meineserachtens sehr Aussagekräftig aufgebaut sind. Hinzu kommt die Abwärtskompatibilität, die in viele anderen Formaten zum Problem führt, aber in Google Protobuf sehr einfach, unter beachtung weniger Punkte, zu bewältigen ist. 

## Persönliche Meinung
Ich bin von Protobuf sehr überzeugt und finde es auch sehr einfach zu verwenden. Es muss ausschließlich eine Proto-Message erstellt und mithilfe von dem dazugehörigen Compiler die notwendigen Stubs für die entsprechende Programmiersprache erzeugt werden. Für C# wird ein einfaches NuGet eingebunden, welches das Kompilieren der proto-Dateien übernimmt und dementsprechend in der Umgebung unter dem angegebenen Namespace bereitgestellt wird. 

## Erfahrungswerte LS/IV (von CityGis)
Bei der Anbindung von LS/IV war ich zu Beginn sehr zuversichtlich, dass eine schnelle Anbindung an die Schnittstelle möglich ist. Mir wurden die Proto-Messages und eine Dokumentation zur Verfügung gestellt. Zusätzlich wird von LS/IV eine Test-Umgebung zur Verfügung gestellt. Das heißt, die Kommunikation konnte mit Test-Daten ohne Einfluss auf eine Produktions-Umgebung getestet werden. Nach einigen Tests und vielen Stunden hat sich herausgestellt, dass die Proto-Messages und Dokumentation nicht zu 100% übereinstimmen. Zum Teil wurden Felder entfernt und einfach verändert, sodass eine Kompatibilität in gar keinem Fall gewährleistet ist, weshalb es auf meiner Seite oft zu Exceptions gekommen ist, weil ich unwissentlich das falsche Nachrichten-Format verwendet habe. Erschwerlicherweise kam hinzu, dass CityGis in dem Nachrichten-Format die Holländische Sprache verwendet hat. Grundsätzlich hätte ich alle Felder der Nachrichten auf englisch übersetzen können und es hätte dennoch funktioniert. Das Problem, das ich damit habe, ist der Aufwand der Umbennenung der eigentlich nicht sein sollte. Ein weiterer Punkt der beim Wechsel auf die Produktions-Umgebung zum Problem geführt hat, ist das in der Dokumentation definiert wurde, dass bestimmte Felder in jedem Fall übertragen werden, aber in Wirklichkeit Fälle existieren, in denen das nicht Zutrifft. Allerdings ist das ein Problem, das bei jeder Schnittstelle auftreten kann. 

## FAQ
### Warum erforderlich und optional in Protobuf 3 entfernt wurde?
Quelle: https://www.it-swarm.com.de/de/protocol-buffers/warum-erforderlich-und-optional-ist-protokollpuffer-3-entfernt/1054474949/
> Der Nutzen von required stand im Mittelpunkt vieler Debatten und Flammenkriege. Auf beiden Seiten gab es große Lager. Ein Lager mochte es zu garantieren, dass ein Wert vorhanden war und bereit war, mit seinen Einschränkungen zu leben, aber das andere Lager fühlte sich required gefährlich oder nicht hilfreich an, da es nicht sicher hinzugefügt oder entfernt werden kann.
Lassen Sie mich näher erläutern, warum required -Felder sparsam verwendet werden sollten. Wenn Sie bereits ein Proto verwenden, können Sie kein erforderliches Feld hinzufügen, da alte Anwendungen dieses Feld nicht bereitstellen und Anwendungen den Fehler im Allgemeinen nicht gut behandeln. Sie können sicherstellen, dass zuerst alle alten Anwendungen aktualisiert werden, aber es kann leicht sein, einen Fehler zu machen, und es hilft nichts, wenn Sie die Protos im any -Datenspeicher speichern (auch von kurzer Dauer, wie z. B. memcached). Gleiches gilt, wenn ein erforderliches Feld entfernt wird.
Viele Pflichtfelder waren "offensichtlich" erforderlich, bis ... sie es nicht waren. Angenommen, Sie haben ein id - Feld für eine Get - Methode. Das ist offensichtlich erforderlich. Außer, dass Sie später möglicherweise das id von int in string oder von int32 in int64 ändern müssen. Dazu muss ein neues muchBetterId - Feld hinzugefügt werden. Nun verbleibt das alte id - Feld, das muss angegeben werden muss, aber schließlich vollständig ignoriert wird.
Wenn diese beiden Probleme kombiniert werden, wird die Anzahl der nützlichen Felder required begrenzt und die Lager streiten darüber, ob sie noch Wert haben. Die Gegner von required waren nicht unbedingt gegen die Idee, sondern deren aktuelle Form. Einige schlugen vor, eine aussagekräftigere Validierungsbibliothek zu entwickeln, die required zusammen mit etwas Fortgeschrittenerem wie name.length > 10 Überprüfen und gleichzeitig ein besseres Fehlermodell sicherstellen könnte.
Insgesamt scheint Proto3 die Einfachheit zu fördern, und das Entfernen von required ist einfacher. Aber vielleicht überzeugender: Das Entfernen von required machte für proto3 Sinn, wenn es mit anderen Funktionen kombiniert wurde, z. B. dem Entfernen der Feldpräsenz für Grundelemente und dem Entfernen überschreibender Standardwerte.
Ich bin kein Protobuf-Entwickler und in keiner Weise maßgebend in diesem Bereich, aber ich hoffe trotzdem, dass die Erklärung nützlich ist.

Statement von Protobuf Entwicklern:
> Wir haben erforderliche Felder in proto3 gelöscht, da erforderliche Felder im Allgemeinen als schädlich eingestuft werden und die Kompatibilitätssemantik von protobuf verletzen. Die Idee bei der Verwendung von protobuf ist, dass Sie Felder zu Ihrer Protokolldefinition hinzufügen/daraus entfernen können, während Sie mit neueren/älteren Binärdateien weiterhin vollständig vorwärts-/rückwärtskompatibel sind. Erforderliche Felder unterbrechen dies jedoch. Sie können einer .proto-Definition weder ein erforderliches Feld sicher hinzufügen noch ein vorhandenes erforderliches Feld sicher entfernen, da beide Aktionen die Kabelkompatibilität unterbrechen. Wenn Sie beispielsweise einer .proto-Definition ein erforderliches Feld hinzufügen, können Binärdateien, die mit der neuen Definition erstellt wurden, keine mit der alten Definition serialisierten Daten analysieren, da das erforderliche Feld in alten Daten nicht vorhanden ist. In einem komplexen System, in dem .proto-Definitionen von vielen verschiedenen Systemkomponenten gemeinsam verwendet werden, kann das Hinzufügen/Entfernen von erforderlichen Feldern leicht mehrere Teile des Systems zum Absturz bringen. Wir haben mehrere Male Produktionsprobleme gesehen, die dadurch verursacht wurden. In Google ist es praktisch überall verboten, erforderliche Felder hinzuzufügen oder zu entfernen. Aus diesem Grund haben wir Pflichtfelder in proto3 komplett entfernt.
Nach dem Entfernen von "erforderlich" ist "optional" nur redundant, sodass wir auch "optional" entfernt haben.


# gRPC

## Was ist gRPC?
gRPC (gRPC **R**emote **P**rocedure **C**all) ist ein Protokoll das zum Aufruf von Funktionen in verteilten Computersystem Anwendung findet (https://grpc.io/docs/what-is-grpc/introduction/). Es bassiert auf HTTP/2 und Google Protobuf. In gRPC wurde die Message-Definition von Protobuf um eine Service-Definition erweitert. Das heißt, es wird mithilfe der Syntax von Protobuf 3 ein Service definiert. Dieser gilt für Server und Client. Je nachdem welcher Teil implementiert wird, muss angegeben werden, ob es sich um Server oder Client handelt, damit der richtige Stub erzeugt werden kann. 

## Kernkonzepte von gRPC
Das Ziel von gRPC ist die Definition eines Services mithilfe von einer Schnittstellen-Beschreibungssprache (https://grpc.io/docs/what-is-grpc/core-concepts/). 

Das Beispiel stammt von der oben genannten offiziellen Seite von gRPC.

```protobuf
service HelloService {
  rpc SayHello (HelloRequest) returns (HelloResponse);
}

message HelloRequest {
  string greeting = 1;
}

message HelloResponse {
  string reply = 1;
}
```

### Unterschiedliche Typen von Service-Anfragen
#### Einfache Anfrage mit Antwort (Synchron)
Der Client schickt dem Server eine Anfrage und wartet dementsprechend auf die Antwort vom Serrver (synchron).

```protobuf
rpc SayHello(HelloRequest) returns (HelloResponse);
```

#### Server streamt Daten zu Client (Asynchron)
Der Client stellt eine Anfrage an den Server und der Server Antwortet mit einem Stream, auf dem der Client dann hören kann, bis die Kommunikation durch den Server beendet wird oder die Verbindung abbricht. Beispielsweise könnte in dem `HelloRequest` eine Information enthalten sein, die den Server auffordert den Clienten mit bestimmten Nachrichten zu versorgen (Client möchte bspw. nur englische Nachrichten).

```protobuf
rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse);
```

#### Client stream Daten zu Server (Asynchron)
Der Client schickt dem Server einen Stream und kann dann über einen beliebigen Zeitraum den Stream offen halten bis der Client mit dem Senden der Daten fertig ist oder die Verbindung abbricht. 

```protobuf
rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse);
```

#### Beideitiges streamen von Daten Client <=> Server
Als vierte Variante gibt es auch die Bi-Direktionale kommunikation. Diese ermöglicht beiden Teilen, Server und Client, asynchron Nachrichten zu verschicken und zu erhalten. Die beiden streams sind unabhängig von einander und es können belibige Nachrichten ausgetauscht werden. Die Reihenfolge der Nachrichten wird in jedem Stream unabhängig beachtet.

```protobuf
rpc BidiHello(stream HelloRequest) returns (stream HelloResponse);
```

### Timeouts/Zeitlimit
gRPC erlaubt es den Clienten ein Zeitlimit für die Anfrage zu setzen. Das heißt, wenn eine Anfrage begrenzt ist und der Server nicht rechtezeitig antwortet, dann wird der request auf Client-Seite beendet und dem Clienten wird eine Exception geworfen. Auf Server Seite hingegen, kann geprüft werden ob der Request bereits vom CLient abgebrochen wurde bzw. wie viel Zeit bis zum Zeitlimit verbleibt. 

### Metadaten
Jeder RPC-Aufruf enthält Metadaten (wie bspw. für die Autentifikation) in Form von Key-Value Paaren. Die Schlüssel sind typischerweise strings und die werte ebenfalls, wobei die Werte auch Binär-Daten akzeptieren würden.

### Kanäle
Ein gRPC Kanal stellt eine Verbindung zu einem gRPC Server zur Verfügung. Damit wäre der Client in der Lage diverse Kanal-Einstellungen zu verändern, wie bspw. die Nachrichten-Kompression. Zusätzlich stellen einige implementierungen die Status-Abfrage von einem Kanal zur Verfügung (`Connected` oder `Idle`)

## Beispiel
Im folgenden ein Beispiel mit entsprechenden Client/Server-stubs (Beispiel von https://grpc.io/docs/languages/csharp/quickstart/).

### Service-Definition
```protobuf
// The greeting service definition.
service Greeter {
  // Sends a greeting
  rpc SayHello (HelloRequest) returns (HelloReply) {}
  // Sends another greeting
  rpc SayHelloAgain (HelloRequest) returns (HelloReply) {}
}

// The request message containing the user's name.
message HelloRequest {
  string name = 1;
}

// The response message containing the greetings
message HelloReply {
  string message = 1;
}
```

### Server-Implementation
Im folgedenen wird `Greeter.GreeterBase` als Basisklasse verwendet, die vom Compiler automatisch erzeugt wurde. 

```csharp
class GreeterImpl : Greeter.GreeterBase
{
    // Server side handler of the SayHello RPC
    public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
    {
        return Task.FromResult(new HelloReply { Message = "Hello " + request.Name });
    }

    // Server side handler for the SayHelloAgain RPC
    public override Task<HelloReply> SayHelloAgain(HelloRequest request, ServerCallContext context)
    {
        return Task.FromResult(new HelloReply { Message = "Hello again " + request.Name });
    }
}
```

### Client-Implementation
Auf Client-Seite wurde ebenfalls automatisch ein Stub generiert, der mithilfe der URL vom Server vollständig funktionsfähig ist. 

```csharp
public static void Main(string[] args)
{
    Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);

    var client = new Greeter.GreeterClient(channel);
    String user = "you";

    var reply = client.SayHello(new HelloRequest { Name = user });
    Console.WriteLine("Greeting: " + reply.Message);

    var secondReply = client.SayHelloAgain(new HelloRequest { Name = user });
    Console.WriteLine("Greeting: " + secondReply.Message);

    channel.ShutdownAsync().Wait();
    Console.WriteLine("Press any key to exit...");
    Console.ReadKey();
}
```
## Warum sollte gRPC eingesetzt werden?
gRPC basiert auf einer unabhängigen Beschreibungssprache und ist damit vollkommen flexibel einsetzbar. Die offizielle Unterstüzung erstreckt sich über sämtliche Programmiersprachen wie C#, C++, Dart, Go, Java, Kotlin, Node, Objective-C, PHP, Python und Ruby. Durch das darunterliegende HTTP/2 Protokoll basiert es bereits auf einem aktuellen Standard und wurde für die Kommunikation innerhalb von einem Service-Mesh ausgelegt. Eine horizontale Skalierung ist ohne weiteres möglich. 