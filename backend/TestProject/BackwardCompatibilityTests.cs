﻿using Benchmarking;
using Google.Protobuf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProject
{
    [TestClass]
    public class BackwardCompatibilityTests
    {
        [TestMethod]
        public void ParseOldToNew()
        {
            Addressbook old = new Addressbook();
            var oldPerson = new Person
            {
                Id = 1,
                FirstName = "John",
                Name = "Doe",
                Email = "john.doe@unknown.com",
            };
            oldPerson.Numbers.Add(new Person.Types.TelephoneNumber { Description = "Work", TelephoneNumber_ = "1234/7382947" });
            old.Persons.Add(oldPerson);

            var oldBinaryData = old.ToByteArray();

            // now parse old data with new type
            BackwardsExample.Addressbook newBook = BackwardsExample.Addressbook.Parser.ParseFrom(oldBinaryData);

            Assert.AreEqual(1, newBook.Persons.Count);
            var firstPerson = newBook.Persons.First();
            Assert.AreEqual(oldPerson.Id, firstPerson.Id);
            Assert.AreEqual(oldPerson.FirstName, firstPerson.FirstName);
            Assert.AreEqual(oldPerson.Name, firstPerson.Name);
            Assert.AreEqual(oldPerson.Email, firstPerson.Email);
            Assert.AreEqual(1, firstPerson.Numbers.Count);
            var firstNumber = firstPerson.Numbers.First();
            // Description is not known in the new type
            Assert.AreEqual(BackwardsExample.Person.Types.TelephoneNumber.Types.NumberType.Default, firstNumber.NumberType); // Contains the default value
            Assert.AreEqual("Work", firstNumber.OBSOLETEDescription); // Contains the default value
            Assert.AreEqual("1234/7382947", firstNumber.TelephoneNumber_);
        }

        [TestMethod]
        public void ParseNewToOld()
        {
            BackwardsExample.Addressbook newBook = new BackwardsExample.Addressbook();
            var newPerson = new BackwardsExample.Person
            {
                Id = 1,
                FirstName = "John",
                Name = "Doe",
                Email = "john.doe@unknown.com",
            };
            newPerson.Numbers.Add(new BackwardsExample.Person.Types.TelephoneNumber { 
                NumberType = BackwardsExample.Person.Types.TelephoneNumber.Types.NumberType.Mobile, 
                TelephoneNumber_ = "1234/7382947" 
            });
            newBook.Persons.Add(newPerson);

            var newBinaryData = newBook.ToByteArray();

            // now parse new data with old type
            Addressbook oldBook = Addressbook.Parser.ParseFrom(newBinaryData);

            Assert.AreEqual(1, oldBook.Persons.Count);
            var firstPerson = oldBook.Persons.First();
            Assert.AreEqual(newPerson.Id, firstPerson.Id);
            Assert.AreEqual(newPerson.FirstName, firstPerson.FirstName);
            Assert.AreEqual(newPerson.Name, firstPerson.Name);
            Assert.AreEqual(newPerson.Email, firstPerson.Email);
            Assert.AreEqual(1, firstPerson.Numbers.Count);
            var firstNumber = firstPerson.Numbers.First();
            // Description should be empty => not know in the new format
            Assert.AreEqual(string.Empty, firstNumber.Description); // Contains the default value
            Assert.AreEqual("1234/7382947", firstNumber.TelephoneNumber_);
        }
    }
}
