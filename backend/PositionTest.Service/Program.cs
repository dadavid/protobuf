﻿using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using MessageDispatcher.Core;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using static MessageDispatcher.Core.DispatchingService;

namespace PositionTest.Service
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var httpHandler = new HttpClientHandler
            {
                // Return `true` to allow certificates that are untrusted/invalid
                ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var channel = GrpcChannel.ForAddress("https://localhost:5005", new GrpcChannelOptions { HttpHandler = httpHandler });
            var client = new DispatchingServiceClient(channel);

            using var streamingCall = client.GetServiceMessages(new ServiceInfo { ServiceName = "TestService", Version = 1 });

            try
            {
                await foreach (var serviceData in streamingCall.ResponseStream.ReadAllAsync())
                {
                    Console.WriteLine($"{serviceData.MessageTypeName} | {serviceData.Payload.ToStringUtf8()}");
                }
            }
            catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
            {
                Console.WriteLine("Stream cancelled.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
