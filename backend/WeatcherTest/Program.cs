﻿using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static MessageDispatcher.Core.DispatchingService;

namespace WeatcherTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var httpHandler = new HttpClientHandler
            {
                // Return `true` to allow certificates that are untrusted/invalid
                ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var channel = GrpcChannel.ForAddress("https://localhost:5005", new GrpcChannelOptions { HttpHandler = httpHandler });
            var client = new DispatchingServiceClient(channel);

            try
            {
                var test = client.StreamMessages();
                for (int i = 0; i < 100; i++)
                {
                    await test.RequestStream.WriteAsync(new MessageDispatcher.Core.MiddlewareMessage
                    {
                        ServiceName = "TestService",
                        Version = 1,
                        MessageTypeName = "Test",
                        Payload = ByteString.CopyFromUtf8($"Hallo Welt {i}")
                    });
                    Console.WriteLine("Data Pushed");
                    await Task.Delay(500);
                }
                await test.RequestStream.CompleteAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
